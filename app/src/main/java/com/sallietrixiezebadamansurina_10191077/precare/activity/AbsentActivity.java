package com.sallietrixiezebadamansurina_10191077.precare.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.sallietrixiezebadamansurina_10191077.precare.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AbsentActivity extends AppCompatActivity {

    LinearLayout backButton;
    EditText et_date, et_absent_keterangan;
    AutoCompleteTextView absent_type;
    Button btn_absent;
    ProgressBar pb_absent;

    DatePickerDialog datePickerDialog;
    SimpleDateFormat simpleDateFormat;
    ArrayAdapter<String> arrayAdapter;
    SharedPreferences sharedPreferences;

    String date;
    String type;
    String[] absentType = {"Sick", "Official travel", "Holiday"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_absent);

        findViewandVar();
        initView();
        onClickListener();
    }

    private void findViewandVar() {
        backButton = findViewById(R.id.ll_absent_back);
        et_date = findViewById(R.id.et_date);
        absent_type = findViewById(R.id.absent_type);
        btn_absent = findViewById(R.id.btn_absent);
        et_absent_keterangan = findViewById(R.id.et_absent_keterangan);
        pb_absent = findViewById(R.id.absent_pb);
        arrayAdapter = new ArrayAdapter<String>(this, R.layout.item_absentype, absentType);
        simpleDateFormat = new SimpleDateFormat("yyy-MM-dd");
        sharedPreferences = getSharedPreferences(getString(R.string.sp), MODE_PRIVATE);
    }

    private void initView() {
        absent_type.setAdapter(arrayAdapter);
    }

    private void onClickListener() {
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        et_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateDialog();
            }
        });

        absent_type.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                type = parent.getItemAtPosition(position).toString();
            }
        });
        btn_absent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendAbsent();
            }
        });

    }

    private void showDateDialog() {
        Calendar calendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, month, dayOfMonth);
                et_date.setText(simpleDateFormat.format(newDate.getTime()));
                date = simpleDateFormat.format(newDate.getTime());
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    private void sendAbsent() {
        pb_absent.setVisibility(View.VISIBLE);
        String url = getString(R.string.api_url) + "absent";
        JSONObject data = new JSONObject();
        try {
            data.put("user_id", sharedPreferences.getString("user_id", null));
            data.put("tipe", type);
            data.put("tanggal", date);
            data.put("keterangan", et_absent_keterangan.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, data,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if(response.getString("status").equals("gagal")) {
                                Toast.makeText(AbsentActivity.this, response.getString("keterangan"), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(AbsentActivity.this, response.getString("keterangan"), Toast.LENGTH_SHORT).show();
                                et_date.setText("");
                                et_absent_keterangan.setText("");
                                absent_type.setText("");
                                finish();
                            }
                            pb_absent.setVisibility(View.GONE);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(AbsentActivity.this, "Failed to get request", Toast.LENGTH_SHORT).show();
                pb_absent.setVisibility(View.GONE);
            }
        });
        RequestQueue rq = Volley.newRequestQueue(this);
        rq.add(request);
    }
}