package com.sallietrixiezebadamansurina_10191077.precare.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.sallietrixiezebadamansurina_10191077.precare.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class CekinActivity extends AppCompatActivity {

    LinearLayout ll_back, cekin_lokasi, cekin_cekin;
    TextView cekin_nama, cekin_nip, cekin_waktu, cekin_lat, cekin_lng;
    CircleImageView cekin_img;
    ProgressBar cekin_pb;

    FusedLocationProviderClient fusedLocationProviderClient;
    double latitude, longitude;

    Dialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cekin);

        ll_back = findViewById(R.id.ll_cekin_back);
        cekin_lokasi = findViewById(R.id.cekin_lokasi);
        cekin_cekin = findViewById(R.id.cekin_cekin);
        cekin_img = findViewById(R.id.cekin_img);
        cekin_nama = findViewById(R.id.cekin_nama);
        cekin_nip = findViewById(R.id.cekin_nip);
        cekin_waktu = findViewById(R.id.cekin_waktu);
        cekin_lat = findViewById(R.id.cekin_lat);
        cekin_lng = findViewById(R.id.cekin_lng);
        cekin_pb = findViewById(R.id.cekin_pb);

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(CekinActivity.this);
        dialog = new Dialog(this);

        getCekinStatus();

        cekin_cekin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cekIn();
            }
        });

        ll_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void getCekinStatus() {
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.sp), MODE_PRIVATE);
        String url = getString(R.string.api_url) + "get-cekin-status/" + sharedPreferences.getString("user_id", null);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Glide.with(CekinActivity.this).load(getString(R.string.api_url_storage) + response.getJSONObject("user").getString("foto")).into(cekin_img);
                            cekin_nama.setText(response.getJSONObject("user").getString("name"));
                            cekin_nip.setText(response.getJSONObject("user").getString("nip"));
                            if (response.getString("status").equals("berhasil")) {
                                if (response.getString("waktu").equals("Auto Record (Late)")) {
                                    cekin_waktu.setTextColor(getResources().getColor(R.color.salmon));
                                    cekin_lat.setTextColor(getResources().getColor(R.color.salmon));
                                    cekin_lng.setTextColor(getResources().getColor(R.color.salmon));
                                } else {
                                    cekin_lokasi.setVisibility(View.VISIBLE);
                                }
                                cekin_waktu.setText(response.getString("waktu"));
                                cekin_lat.setText("Latitude : " + response.getJSONObject("data").getString("latitude"));
                                cekin_lng.setText("Longitude : " + response.getJSONObject("data").getString("longitude"));
                                cekin_cekin.setVisibility(View.GONE);

                                cekin_lokasi.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(Intent.ACTION_VIEW);
                                        try {
                                            intent.setData(Uri.parse("geo:"+response.getJSONObject("data").getString("latitude")+","+response.getJSONObject("data").getString("longitude")+"?q="+response.getJSONObject("data").getString("latitude")+","+response.getJSONObject("data").getString("longitude")));
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        Intent chooser = Intent.createChooser(intent, "Launch Maps");
                                        startActivity(chooser);
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                            cekin_pb.setVisibility(View.GONE);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(CekinActivity.this, "Failed to get request", Toast.LENGTH_SHORT).show();
                cekin_pb.setVisibility(View.GONE);
            }
        });
        RequestQueue rq = Volley.newRequestQueue(this);
        rq.add(request);
    }

    private void cekIn() {
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.sp), MODE_PRIVATE);
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            fusedLocationProviderClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                @Override
                public void onComplete(@NonNull Task<Location> task) {
                    // Inisialisasi Location
                    Location location = task.getResult();
                    if (location != null) {
                        try {
                            Geocoder geocoder = new Geocoder(CekinActivity.this, Locale.getDefault());
                            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);

                            latitude = addresses.get(0).getLatitude();
                            longitude = addresses.get(0).getLongitude();

                            float[] results = new float[1];
                            Location.distanceBetween(0.12658590772598052, 117.4846226129873, latitude, longitude, results);
                            float distanceInMeters = results[0];
                            boolean isWithin1km = distanceInMeters < 300;

//                          QUERY CEKIN
                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject.put("user_id", sharedPreferences.getString("user_id", null));
                                jsonObject.put("latitude", latitude);
                                jsonObject.put("longitude", longitude);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            String url = getString(R.string.api_url) + "cekin";
                            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                                    new Response.Listener<JSONObject>() {
                                        @Override
                                        public void onResponse(JSONObject response) {
                                            try {
                                                if (response.getString("status").equals("berhasil")) {
                                                    successCekinDialog();
                                                } else {
                                                    Toast.makeText(CekinActivity.this, response.getString("keterangan"), Toast.LENGTH_SHORT).show();
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Toast.makeText(CekinActivity.this, "Failed to get request", Toast.LENGTH_SHORT).show();
                                }
                            });
                            if(isWithin1km) {
                                RequestQueue rq = Volley.newRequestQueue(CekinActivity.this);
                                rq.add(request);
                            } else {
                                Toast.makeText(CekinActivity.this, "Your location is " + String.valueOf(distanceInMeters-50) + " meters from office", Toast.LENGTH_SHORT).show();
                            }
//                          ENDQUERYCEKIN
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } else {
            ActivityCompat.requestPermissions(CekinActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 44);
        }
    }

    private void successCekinDialog() {
        dialog.setContentView(R.layout.dialog_cekin_success);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Button btnOk = dialog.findViewById(R.id.btn_dialog_ok);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                overridePendingTransition(0, 0);
                finish();
                overridePendingTransition(0, 0);
                startActivity(getIntent());
                overridePendingTransition(0, 0);
            }
        });
        dialog.show();
    }

}