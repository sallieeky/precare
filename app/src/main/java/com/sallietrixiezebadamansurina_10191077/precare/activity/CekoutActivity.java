package com.sallietrixiezebadamansurina_10191077.precare.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.sallietrixiezebadamansurina_10191077.precare.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class CekoutActivity extends AppCompatActivity {

    CircleImageView iv_foto;
    TextView tv_nama, tv_nip, tv_waktu, tv_lat, tv_lng;
    LinearLayout btn_lokasi, btn_cekout, btn_back;
    ProgressBar cekout_pb;
    EditText et_keterangan;

    SharedPreferences sharedPreferences;
    FusedLocationProviderClient fusedLocationProviderClient;
    double latitude, longitude;

    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cekout);

        findViewAndVar();
        onClickListener();

        getCekoutStatus();

    }

    private void findViewAndVar() {
        iv_foto = findViewById(R.id.cekout_img);
        tv_nama = findViewById(R.id.cekout_nama);
        tv_nip = findViewById(R.id.cekout_nip);
        tv_waktu = findViewById(R.id.cekout_waktu);
        tv_lat = findViewById(R.id.cekout_lat);
        tv_lng = findViewById(R.id.cekout_lng);
        btn_lokasi = findViewById(R.id.cekout_lokasi);
        btn_cekout = findViewById(R.id.cekout_cekout);
        btn_back = findViewById(R.id.ll_cekout_back);
        cekout_pb = findViewById(R.id.cekout_pb);
        et_keterangan = findViewById(R.id.et_cekout_keterangan);

        sharedPreferences = getSharedPreferences(getString(R.string.sp), MODE_PRIVATE);
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(CekoutActivity.this);
        dialog = new Dialog(this);

    }

    private void onClickListener() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_cekout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cekOut();
            }
        });
    }

    private void getCekoutStatus() {
        String url = getString(R.string.api_url) + "get-cekout-status/" + sharedPreferences.getString("user_id", null);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Glide.with(CekoutActivity.this).load(getString(R.string.api_url_storage) + response.getJSONObject("user").getString("foto")).into(iv_foto);
                            tv_nama.setText(response.getJSONObject("user").getString("name"));
                            tv_nip.setText(response.getJSONObject("user").getString("nip"));
                            if (response.getString("status").equals("berhasil")) {
                                if (response.getString("waktu").equals("Auto Record (Late)") ) {
                                    tv_waktu.setTextColor(getResources().getColor(R.color.salmon));
                                    tv_lat.setTextColor(getResources().getColor(R.color.salmon));
                                    tv_lng.setTextColor(getResources().getColor(R.color.salmon));
                                } else {
                                    btn_lokasi.setVisibility(View.VISIBLE);
                                }
                                tv_waktu.setText(response.getString("waktu"));
                                tv_lat.setText("Latitude : " + response.getJSONObject("data").getString("latitude"));
                                tv_lng.setText("Longitude : " + response.getJSONObject("data").getString("longitude"));
                                et_keterangan.setText(response.getJSONObject("data").getString("kegiatan"));
                                et_keterangan.setEnabled(false);
                                btn_cekout.setVisibility(View.GONE);

                                btn_lokasi.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(Intent.ACTION_VIEW);
                                        try {
                                            intent.setData(Uri.parse("geo:"+response.getJSONObject("data").getString("latitude")+","+response.getJSONObject("data").getString("longitude")+"?q="+response.getJSONObject("data").getString("latitude")+","+response.getJSONObject("data").getString("longitude")));
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        Intent chooser = Intent.createChooser(intent, "Launch Maps");
                                        startActivity(chooser);
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        cekout_pb.setVisibility(View.GONE);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(CekoutActivity.this, "Failed to get request", Toast.LENGTH_SHORT).show();
                cekout_pb.setVisibility(View.GONE);
            }
        });
        RequestQueue rq = Volley.newRequestQueue(this);
        rq.add(request);
    }

    private void cekOut() {
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            fusedLocationProviderClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                @Override
                public void onComplete(@NonNull Task<Location> task) {
                    // Inisialisasi Location
                    Location location = task.getResult();
                    if (location != null) {
                        try {
                            Geocoder geocoder = new Geocoder(CekoutActivity.this, Locale.getDefault());
                            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);

                            latitude = addresses.get(0).getLatitude();
                            longitude = addresses.get(0).getLongitude();

                            float[] results = new float[1];
                            Location.distanceBetween(0.12658590772598052, 117.4846226129873, latitude, longitude, results);
                            float distanceInMeters = results[0];
                            boolean isWithin1km = distanceInMeters < 300;

//                          QUERY CEKIN
                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject.put("user_id", sharedPreferences.getString("user_id", null));
                                jsonObject.put("latitude", latitude);
                                jsonObject.put("longitude", longitude);
                                jsonObject.put("kegiatan", et_keterangan.getText().toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            String url = getString(R.string.api_url) + "cekout";
                            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                                    new Response.Listener<JSONObject>() {
                                        @Override
                                        public void onResponse(JSONObject response) {
                                            try {
                                                if (response.getString("status").equals("berhasil")) {
                                                    successCekoutDialog();
                                                } else {
                                                    Toast.makeText(CekoutActivity.this, response.getString("keterangan"), Toast.LENGTH_SHORT).show();
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Toast.makeText(CekoutActivity.this, "Failed to get request", Toast.LENGTH_SHORT).show();
                                }
                            });
                            if(isWithin1km) {
                                RequestQueue rq = Volley.newRequestQueue(CekoutActivity.this);
                                rq.add(request);
                            } else {
                                Toast.makeText(CekoutActivity.this, "Your location is " + String.valueOf(distanceInMeters-50) + " meters from office", Toast.LENGTH_SHORT).show();
                            }
//                          ENDQUERYCEKIN
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }
            });
        } else {
            ActivityCompat.requestPermissions(CekoutActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 44);
        }
    }

    private void successCekoutDialog() {
        dialog.setContentView(R.layout.dialog_cekout_success);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Button btnOk = dialog.findViewById(R.id.btn_dialog_ok);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                overridePendingTransition(0, 0);
                finish();
                overridePendingTransition(0, 0);
                startActivity(getIntent());
                overridePendingTransition(0, 0);
            }
        });
        dialog.show();
    }
}