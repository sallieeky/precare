package com.sallietrixiezebadamansurina_10191077.precare.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.sallietrixiezebadamansurina_10191077.precare.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    EditText et_email, et_password;
    TextView tv_login_incorrect;
    Button btn_login;
    ProgressBar pb;
    RequestQueue rq;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        et_email = findViewById(R.id.et_email);
        et_password = findViewById(R.id.et_password);
        tv_login_incorrect = findViewById(R.id.login_incorrect);
        btn_login = findViewById(R.id.btn_login);
        pb = findViewById(R.id.login_progress);

        sharedPreferences = getSharedPreferences(getResources().getString(R.string.sp), MODE_PRIVATE);
        rq = Volley.newRequestQueue(this);

        initOnClick();

    }

    private void initOnClick() {
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tv_login_incorrect.setVisibility(View.INVISIBLE);

                String email = et_email.getText().toString();
                String password = et_password.getText().toString();

                JSONObject data = new JSONObject();
                try {
                    data.put("email", email);
                    data.put("password", password);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                String url = getResources().getString(R.string.api_url) + "login";
                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, data,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                                pb.setVisibility(View.INVISIBLE);
                                btn_login.setEnabled(true);
                                btn_login.setText("LOG IN");

                                try {
                                    if(response.getString("status").equals("berhasil")) {
                                        SharedPreferences.Editor editor = sharedPreferences.edit();
                                        try {
                                            editor.putString("user_id", response.getJSONObject("data").getString("id"));
                                            editor.apply();
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                        startActivity(intent);

                                        finish();
                                    } else {
                                        tv_login_incorrect.setVisibility(View.VISIBLE);
                                    }

                                    et_password.setText("");

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(LoginActivity.this, "Failed to send request", Toast.LENGTH_SHORT).show();
                        pb.setVisibility(View.INVISIBLE);
                        btn_login.setEnabled(true);
                        btn_login.setText("LOG IN");
                    }
                });

                if( TextUtils.isEmpty(et_email.getText()) ) {
                    et_email.setError( "Email field is required!" );
                } else {
                    pb.setVisibility(View.VISIBLE);
                    btn_login.setEnabled(false);
                    btn_login.setText("PLEASE WAIT . . .");
                    rq.add(request);
                }
            }
        });
    }
}