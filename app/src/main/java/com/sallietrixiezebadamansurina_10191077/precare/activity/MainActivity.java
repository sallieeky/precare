package com.sallietrixiezebadamansurina_10191077.precare.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationBarView;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.sallietrixiezebadamansurina_10191077.precare.R;
import com.sallietrixiezebadamansurina_10191077.precare.fragment.AttendanceFragment;
import com.sallietrixiezebadamansurina_10191077.precare.fragment.HelpFragment;
import com.sallietrixiezebadamansurina_10191077.precare.fragment.ProfileFragment;
import com.sallietrixiezebadamansurina_10191077.precare.fragment.ReportFragment;
import com.sallietrixiezebadamansurina_10191077.precare.util.Capture;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    FrameLayout fragment_container;
    FloatingActionButton btn_scanner;
    BottomNavigationView btm_nav_view;
    SharedPreferences sharedPreferences;
    Dialog dialog;

    public String name, foto, nip, email, no_telp, alamat;
    public ProgressBar ma_progress;

    private boolean doubleBackToExitPressedOnce;

    FusedLocationProviderClient fusedLocationProviderClient;
    double latitude, longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragment_container = findViewById(R.id.fragment_container);
        btn_scanner = findViewById(R.id.btn_scanner);
        btm_nav_view = findViewById(R.id.btm_nav_view);
        ma_progress = findViewById(R.id.ma_progress);

        btm_nav_view.setItemRippleColor(null);
        btm_nav_view.setOnItemSelectedListener(navListener);
        btm_nav_view.setOnItemReselectedListener(reselectedListener);


        sharedPreferences = getSharedPreferences(getResources().getString(R.string.sp), MODE_PRIVATE);

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new AttendanceFragment()).commit();

        dialog = new Dialog(this);

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(MainActivity.this);


        btn_scanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator intentIntegrator = new IntentIntegrator(MainActivity.this);
                intentIntegrator.setPrompt("For flash use volume key up");
                intentIntegrator.setBeepEnabled(true);
                intentIntegrator.setOrientationLocked(true);
                intentIntegrator.setCaptureActivity(Capture.class);

                intentIntegrator.initiateScan();
            }
        });

    }

    @Override
    public void onBackPressed() {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
            Toast.makeText(this, "Click back again to exit", Toast.LENGTH_SHORT).show();
            this.doubleBackToExitPressedOnce = true;
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        IntentResult intentResult = IntentIntegrator.parseActivityResult(
                requestCode, resultCode, data
        );

        if (intentResult.getContents() != null) {


//            LOCATION HERE
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                fusedLocationProviderClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        // Inisialisasi Location
                        Location location = task.getResult();
                        if (location != null) {
                            try {
                                Geocoder geocoder = new Geocoder(MainActivity.this, Locale.getDefault());
                                List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);

                                latitude = addresses.get(0).getLatitude();
                                longitude = addresses.get(0).getLongitude();

                                float[] results = new float[1];
                                Location.distanceBetween(0.12658590772598052, 117.4846226129873, latitude, longitude, results);
                                float distanceInMeters = results[0];
                                boolean isWithin1km = distanceInMeters < 300;

//                          QUERY CEKOUT
                                if(intentResult.getContents().equals(getString(R.string.api_url) + "cekout")) {
                                    openCekoutDialog(intentResult.getContents(), distanceInMeters, isWithin1km);
                                } else {


//                          QUERY CEKIN
                                JSONObject jsonObject = new JSONObject();
                                try {
                                    jsonObject.put("user_id", sharedPreferences.getString("user_id", null));
                                    jsonObject.put("latitude", latitude);
                                    jsonObject.put("longitude", longitude);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, intentResult.getContents(), jsonObject,
                                        new Response.Listener<JSONObject>() {
                                            @Override
                                            public void onResponse(JSONObject response) {
                                                try {
                                                    if (response.getString("status").equals("berhasil")) {
                                                        successCekinDialog();
                                                    } else {
                                                        Toast.makeText(MainActivity.this, response.getString("keterangan"), Toast.LENGTH_SHORT).show();
                                                    }
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Toast.makeText(MainActivity.this, "EROR", Toast.LENGTH_SHORT).show();
                                    }
                                });
                                RequestQueue rq = Volley.newRequestQueue(MainActivity.this);
                                rq.add(request);
                                }
//                          ENDQUERYCEKIN
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                });
            } else {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 44);
            }
//            ENDLOCATION
        } else {}
    }


    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    Fragment fragment = null;
                    switch (item.getItemId()) {
                        case R.id.nav_attendance :
                            fragment = new AttendanceFragment();
                            break;
                        case R.id.nav_report :
                            fragment = new ReportFragment();
                            break;
                        case R.id.nav_profile :
                            fragment = new ProfileFragment();
                            break;
                        case R.id.nav_help :
                            fragment = new HelpFragment();
                            break;
                    }
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();
                    return true;
                }
            };

    private NavigationBarView.OnItemReselectedListener reselectedListener = new NavigationBarView.OnItemReselectedListener() {
        @Override
        public void onNavigationItemReselected(@NonNull MenuItem item) {

        }
    };

    private void successCekinDialog() {
        dialog.setContentView(R.layout.dialog_cekin_success);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Button btnOk = dialog.findViewById(R.id.btn_dialog_ok);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                overridePendingTransition(0, 0);
                finish();
                overridePendingTransition(0, 0);
                startActivity(getIntent());
                overridePendingTransition(0, 0);
            }
        });
        dialog.show();
    }

    private void successCekoutDialog() {
        dialog.setContentView(R.layout.dialog_cekout_success);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Button btnOk = dialog.findViewById(R.id.btn_dialog_ok);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                overridePendingTransition(0, 0);
                finish();
                overridePendingTransition(0, 0);
                startActivity(getIntent());
                overridePendingTransition(0, 0);
            }
        });
        dialog.show();
    }

    private void openCekoutDialog(String url, float distanceInMeters, boolean isWithin1km) {
        dialog.setContentView(R.layout.dialog_cekout_kegiatan);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Button btnOk = dialog.findViewById(R.id.btn_cekout_dialog);
        EditText etKegiatan = dialog.findViewById(R.id.et_cekout_dialog);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("user_id", sharedPreferences.getString("user_id", null));
                    jsonObject.put("latitude", latitude);
                    jsonObject.put("longitude", longitude);
                    jsonObject.put("kegiatan", etKegiatan.getText().toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (response.getString("status").equals("berhasil")) {
                                        successCekoutDialog();
                                    } else {
                                        Toast.makeText(MainActivity.this, response.getString("keterangan"), Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this, "EROR", Toast.LENGTH_SHORT).show();
                    }
                });
                RequestQueue rq = Volley.newRequestQueue(MainActivity.this);
                rq.add(request);
            }
        });
        dialog.show();
    }
}