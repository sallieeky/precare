package com.sallietrixiezebadamansurina_10191077.precare.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import com.sallietrixiezebadamansurina_10191077.precare.R;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        String sp = getResources().getString(R.string.sp);

        new Handler(Looper.getMainLooper()).postDelayed(
            new Runnable() {
                public void run() {
                    SharedPreferences sharedPreferences = getSharedPreferences(sp, MODE_PRIVATE);
                    String login_id = sharedPreferences.getString("user_id", null);

                    if(login_id != null) {
                        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                    } else {
                        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                    }
                }
            },
        1000);
    }
}