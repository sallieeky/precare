package com.sallietrixiezebadamansurina_10191077.precare.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sallietrixiezebadamansurina_10191077.precare.R;

import java.util.List;

public class RvReportAdapter extends RecyclerView.Adapter<RvReportAdapter.MyViewHolder> {

    Context context;
    List<String> tanggal, cekin_waktu, cekin_status, cekout_waktu, cekout_status;

    public RvReportAdapter(Context context, List<String> tanggal, List<String> cekin_waktu, List<String> cekin_status, List<String> cekout_waktu, List<String> cekout_status) {
        this.context = context;
        this.tanggal = tanggal;
        this.cekin_waktu = cekin_waktu;
        this.cekin_status = cekin_status;
        this.cekout_waktu = cekout_waktu;
        this.cekout_status = cekout_status;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_report, parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        if(cekin_status.get(position).equals("Late")) {
            holder.tv_cekin_status.setTextColor(context.getResources().getColor(R.color.salmon));
        } else if(cekin_status.get(position).equals("Absent")) {
            holder.tv_cekin_status.setTextColor(context.getResources().getColor(R.color.light_yellow));
        }

        if(cekout_status.get(position).equals("Late")) {
            holder.tv_cekout_status.setTextColor(context.getResources().getColor(R.color.salmon));
        } else if(cekout_status.get(position).equals("Absent")) {
            holder.tv_cekout_status.setTextColor(context.getResources().getColor(R.color.light_yellow));
        }


        holder.tv_tanggal.setText("Date : " + tanggal.get(position));
        holder.tv_cekin_waktu.setText("Check In : " + cekin_waktu.get(position));
        holder.tv_cekin_status.setText(cekin_status.get(position));
        holder.tv_cekout_waktu.setText("Check Out : " + cekout_waktu.get(position));
        holder.tv_cekout_status.setText(cekout_status.get(position));
    }

    @Override
    public int getItemCount() {
        return tanggal.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tv_tanggal, tv_cekin_waktu, tv_cekin_status, tv_cekout_waktu, tv_cekout_status;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_tanggal = itemView.findViewById(R.id.report_date);
            tv_cekin_waktu = itemView.findViewById(R.id.report_cekin_waktu);
            tv_cekin_status = itemView.findViewById(R.id.report_cekin_status);
            tv_cekout_waktu = itemView.findViewById(R.id.report_cekout_waktu);
            tv_cekout_status = itemView.findViewById(R.id.report_cekout_status);
        }
    }
}
