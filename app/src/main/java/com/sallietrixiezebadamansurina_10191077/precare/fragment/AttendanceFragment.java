package com.sallietrixiezebadamansurina_10191077.precare.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.sallietrixiezebadamansurina_10191077.precare.R;
import com.sallietrixiezebadamansurina_10191077.precare.activity.AbsentActivity;
import com.sallietrixiezebadamansurina_10191077.precare.activity.CekinActivity;
import com.sallietrixiezebadamansurina_10191077.precare.activity.CekoutActivity;
import com.sallietrixiezebadamansurina_10191077.precare.activity.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;


public class AttendanceFragment extends Fragment {

    MainActivity activity;
    SharedPreferences sharedPreferences;
    String nama, nip, foto;
    TextView tv_nama, tv_nama2, tv_nip, att_cekin, att_cekout;
    CircleImageView iv_foto;
    ProgressBar ma_progress;

    CardView cv_cekin, cv_absent, cv_cekout;

    RequestQueue rq;



    public AttendanceFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_attendance, container, false);

        rq = Volley.newRequestQueue(getContext());

        activity = (MainActivity) getActivity();

        tv_nama = view.findViewById(R.id.att_nama);
        tv_nama2 = view.findViewById(R.id.att_nama2);
        tv_nip = view.findViewById(R.id.att_nip);
        iv_foto = view.findViewById(R.id.att_foto);
        att_cekin = view.findViewById(R.id.att_cekin);
        att_cekout = view.findViewById(R.id.att_cekout);

        cv_cekin = view.findViewById(R.id.cv_cekin);
        cv_cekin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CekinActivity.class);
                startActivity(intent);
            }
        });

        cv_absent = view.findViewById(R.id.cv_absent);
        cv_absent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), AbsentActivity.class);
                startActivity(intent);
            }
        });

        cv_cekout = view.findViewById(R.id.cv_cekout);
        cv_cekout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CekoutActivity.class);
                startActivity(intent);
            }
        });

        ma_progress = activity.ma_progress;

        sharedPreferences = activity.getSharedPreferences(getResources().getString(R.string.sp), Context.MODE_PRIVATE);

        getUserData();

        return view;
    }

    private void getUserData() {
        ma_progress.setVisibility(View.VISIBLE);

        String url = getResources().getString(R.string.api_url) + "get-user-cekin-cekout/" + sharedPreferences.getString("user_id", null);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            nama = response.getJSONObject("user").getString("name");
                            nip = response.getJSONObject("user").getString("nip");
                            foto = response.getJSONObject("user").getString("foto");

                            tv_nama.setText(nama);
                            tv_nama2.setText(nama);
                            tv_nip.setText(nip);
                            Glide.with(getContext()).load(getString(R.string.api_url_storage) + foto).into(iv_foto);

                            att_cekin.setText(response.getJSONObject("data").getJSONObject("cekin").getString("jam"));
                            att_cekout.setText(response.getJSONObject("data").getJSONObject("cekout").getString("jam"));


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        ma_progress.setVisibility(View.GONE);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Failed to get request", Toast.LENGTH_SHORT).show();
                ma_progress.setVisibility(View.GONE);
            }
        });
        rq.add(request);
    }
}