package com.sallietrixiezebadamansurina_10191077.precare.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.sallietrixiezebadamansurina_10191077.precare.R;
import com.sallietrixiezebadamansurina_10191077.precare.activity.LoginActivity;
import com.sallietrixiezebadamansurina_10191077.precare.activity.MainActivity;
import com.sallietrixiezebadamansurina_10191077.precare.util.EditProfileAddressDialog;
import com.sallietrixiezebadamansurina_10191077.precare.util.EditProfileNumberDialog;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileFragment extends Fragment {

    LinearLayout edit_address, edit_number;
    TextView tv_nama, tv_nip, tv_nama2, tv_email, tv_alamat, tv_no_telp;
    CircleImageView iv_foto;
    CardView cv_logout;
    String nama, nip, email, alamat, no_telp, foto;

    MainActivity activity;
    ProgressBar ma_progress;

    SharedPreferences sharedPreferences;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        activity = (MainActivity) getActivity();

        tv_nama = view.findViewById(R.id.pr_nama);
        tv_nip = view.findViewById(R.id.pr_nip);
        tv_nama2 = view.findViewById(R.id.pr_nama2);
        tv_email = view.findViewById(R.id.pr_email);
        tv_alamat = view.findViewById(R.id.pr_alamat);
        tv_no_telp = view.findViewById(R.id.pr_no_telp);
        iv_foto = view.findViewById(R.id.pr_foto);
        cv_logout = view.findViewById(R.id.pr_logout);
        edit_address = view.findViewById(R.id.edit_profile_address);
        edit_number = view.findViewById(R.id.edit_profile_number);

        ma_progress = activity.ma_progress;
        sharedPreferences = activity.getSharedPreferences(getString(R.string.sp), Context.MODE_PRIVATE);

        edit_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAddressDialog();
            }
        });

        edit_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openNumberDialog();
            }
        });

        cv_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.remove("user_id");
                editor.apply();

                Intent intent = new Intent(activity, LoginActivity.class);
                startActivity(intent);
                activity.finish();
            }
        });

        getUserData();

        return view;
    }

    private void openAddressDialog() {
        EditProfileAddressDialog dialog = new EditProfileAddressDialog(getContext(), alamat, tv_alamat);
        dialog.show(getActivity().getSupportFragmentManager(), "Edit Address Dialog");
    }
    private void openNumberDialog() {
        EditProfileNumberDialog dialog = new EditProfileNumberDialog(getContext(), no_telp, tv_no_telp);
        dialog.show(getActivity().getSupportFragmentManager(), "Edit Phone Number Dialog");
    }

    private void getUserData() {
        ma_progress.setVisibility(View.VISIBLE);

        String url = getResources().getString(R.string.api_url) + "getuserdata/" + sharedPreferences.getString("user_id", null);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            nama = response.getString("name");
                            nip = response.getString("nip");
                            email = response.getString("email");
                            alamat = response.getString("alamat");
                            no_telp = response.getString("no_telp");
                            foto = response.getString("foto");

                            tv_nama.setText(nama);
                            tv_nip.setText(nip);
                            tv_nama2.setText(nama);
                            tv_email.setText(email);
                            tv_alamat.setText(alamat);
                            tv_no_telp.setText(no_telp);

                            Glide.with(getContext()).load(getString(R.string.api_url_storage) + foto).into(iv_foto);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        ma_progress.setVisibility(View.GONE);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Failed to get request", Toast.LENGTH_SHORT).show();
                ma_progress.setVisibility(View.GONE);
            }
        });
        RequestQueue rq = Volley.newRequestQueue(getContext());
        rq.add(request);
    }
}