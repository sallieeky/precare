package com.sallietrixiezebadamansurina_10191077.precare.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.sallietrixiezebadamansurina_10191077.precare.R;
import com.sallietrixiezebadamansurina_10191077.precare.adapter.RvReportAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ReportFragment extends Fragment {

    RecyclerView rv_report;
    ProgressBar pb;
    List<String> tanggal, cekin_waktu, cekin_status, cekout_waktu, cekout_status;
    SharedPreferences sharedPreferences;


    public ReportFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_report, container, false);

        rv_report = view.findViewById(R.id.rv_report);
        pb = view.findViewById(R.id.report_pb);

        tanggal = new ArrayList<>();
        cekin_waktu = new ArrayList<>();
        cekout_waktu = new ArrayList<>();
        cekin_status = new ArrayList<>();
        cekout_status = new ArrayList<>();
        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.sp), Context.MODE_PRIVATE);

        rv_report.setLayoutManager(new LinearLayoutManager(getContext()));
        getReport();


        return view;
    }

    private void getReport() {
        String url = getString(R.string.api_url) + "get-user-report/" + sharedPreferences.getString("user_id", null);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject data = response.getJSONObject("data");
                            JSONArray cekin = data.getJSONArray("cekin");
                            for (int i=0; i<cekin.length(); i++) {
                                JSONObject cekin_data = cekin.getJSONObject(i);
                                cekin_waktu.add(cekin_data.getString("jam"));
                                cekin_status.add(cekin_data.getString("keterangan"));
                            }
                            JSONArray cekout = data.getJSONArray("cekout");
                            for (int i=0; i<cekout.length(); i++) {
                                JSONObject cekout_data = cekout.getJSONObject(i);
                                tanggal.add(cekout_data.getString("tanggal"));
                                cekout_waktu.add(cekout_data.getString("jam"));
                                cekout_status.add(cekout_data.getString("keterangan"));
                            }

                            RvReportAdapter adapter = new RvReportAdapter(getContext(), tanggal, cekin_waktu, cekin_status, cekout_waktu, cekout_status);
                            rv_report.setAdapter(adapter);

                            pb.setVisibility(View.GONE);
                        } catch (JSONException e) {
                            Toast.makeText(getContext(), e.toString(), Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Failed to get request", Toast.LENGTH_SHORT).show();
                pb.setVisibility(View.GONE);
            }
        });
        RequestQueue rq = Volley.newRequestQueue(getContext());
        rq.add(request);
    }
}