package com.sallietrixiezebadamansurina_10191077.precare.util;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.sallietrixiezebadamansurina_10191077.precare.R;

import org.json.JSONException;
import org.json.JSONObject;

public class EditProfileNumberDialog extends AppCompatDialogFragment {

    EditText et_number;
    TextView tv_number;
    String no_telp;
    Context context;

    SharedPreferences sharedPreferences;

    public EditProfileNumberDialog(Context context, String no_telp, TextView tv_number) {
        this.context = context;
        this.no_telp = no_telp;
        this.tv_number = tv_number;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_profile_number, null);

        et_number = view.findViewById(R.id.dialog_edit_number);
        et_number.setText(no_telp);

        sharedPreferences = context.getSharedPreferences(getString(R.string.sp), Context.MODE_PRIVATE);

        builder.setView(view)
                .setTitle("Phone Number")
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String url = getString(R.string.api_url) + "edit-notelp";
                        JSONObject data = new JSONObject();
                        try {
                            data.put("id", sharedPreferences.getString("user_id", null));
                            data.put("no_telp", et_number.getText().toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, data,
                                new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        try {
                                            if(response.getString("status").equals("berhasil")) {
                                                tv_number.setText(response.getString("no_telp"));
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(context, "Failed to send request", Toast.LENGTH_SHORT).show();
                            }
                        });

                        RequestQueue rq = Volley.newRequestQueue(context);
                        rq.add(request);
                    }
                });
        return builder.create();
    }
}
